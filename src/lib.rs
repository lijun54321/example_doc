//! A short sentence explaining what this (i.e., example_doc) lib is.
//!
//!More detailed explanation about the example_doc library. 
//! The example_lib is used to demo the followings: 
//! - Document this library;
//! - Document public components such as modules, const, functions, etc. in this library (self-explained);
//! - if possible, some code examples about how to use this library/functions;
//! - Refer to some well-documented examples:
//!   - A std lib example: [`std::env`]
//!   - A third-party crate example: [`tokio`]
//!
//! [`std::env`]:  https://doc.rust-lang.org/stable/std/env/index.html
//!
//! [`tokio`]:    https://docs.rs/tokio/1.12.0/tokio/
//! 

extern crate pretty_env_logger;
pub const GREETING: &'static str = "A Rust library example_doc.";
pub mod my_module;
#[macro_use] extern crate log;

/// one sentence to describe the lib_pub_func here 
///
/// Provide a usage example if necessary
/// ```rust
/// #
///  use example_doc;
///  lib_pub_func();
/// #
///
pub fn lib_pub_func() ->() {
    //short inline comment. 
    info!("inside lib_pub_func. const GREETING: {}", GREETING);
}

#[cfg(test)]
mod tests {
    /*  
        multi-line comments
        such as private dev note, not shown on rust-doc page:
    */
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
