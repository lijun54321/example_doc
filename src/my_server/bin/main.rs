/*
The web server is based on following crates:
 - crate [`hyper`] : a fast and correct HTTP implementation written in and for Rust.
 - crate [`warp`] : a super-easy, composable, web server framework for warp speeds

 [`hyper`]: https://docs.rs/hyper/0.14.13/hyper

 [`warp`]: https://docs.rs/warp/0.3.1/warp/

The API/paths includes:
  - GET /plus/:u32/:u32
  - GET /:u16/times/:u16
*/

use warp::Filter;
use chrono::{DateTime, FixedOffset, Local, Utc, SecondsFormat};
extern crate pretty_env_logger;
#[macro_use] extern crate log;

use example_doc;
use example_doc::my_module;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let root = warp::path::end().map(|| "This is the simple API. Try calling /plus/:u32/:u32 or /:u16/times/:u16");
    // use the end() filter to match a shorter path
    let help = warp::path("help")
    // Careful! Omitting the following line would make this filter match
    // requests to /plus/:u32/:u32 and /:u16/times/:u16
    .and(warp::path::end())
    .map(|| "This is the simple API. Try calling /plus/:u32/:u32 or /:u16/times/:u16");

    // GET /plus/:u32/:u32
    let sum = 
        warp::path!("plus" / u32 / u32).map(|a, b| format!("{} + {} = {}", a, b, a + b));
    // GET /:u16/times/:u16
    let times =
        warp::path!(u16 / "times" / u16).map(|a, b| format!("{} times {} = {}", a, b, a * b));
    
    // GET /bye/:string
    let bye = warp::path("bye")
        .and(warp::path::param())
        .map(|name: String| format!("path: /bye with {}!", name));

    let routes = warp::get().and(
        root
            .or(help)
            .or(bye)
            .or(sum)
            .or(times),
    );

    usage();
    my_module::my_pub_func();
    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
}

fn usage() ->() {
    let local_time = Local::now();
    let utc_time = DateTime::<Utc>::from_utc(local_time.naive_utc(), Utc);
    let ca_timezone = FixedOffset::west(7 * 3600);
    let local_t = utc_time.with_timezone(&ca_timezone);
    let local_str = local_t.to_rfc3339_opts(SecondsFormat::Millis, true);

    info!("### server starts ... at {}.  Please test the following URLs (and paths):", local_str);
    debug!("    http://localhost:8000/");
    info!("    http://localhost:8000/help");
    debug!("    http://localhost:8000/plus/2/5");
    debug!("    http://localhost:8000/2/times/5");
    debug!("    http://localhost:8000/bye/goodbye!!!");
}

