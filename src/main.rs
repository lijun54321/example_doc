extern crate pretty_env_logger;
#[macro_use] extern crate log;

use example_doc;
use example_doc::GREETING;
use example_doc::lib_pub_func;
use example_doc::my_module;

fn main()  {
    // init pretty_env_logger
    pretty_env_logger::init();
    
    // use lib's const 
    info!("inside main(), example_doc::GREETING: {}", GREETING);

    // call lib's function
    lib_pub_func();

    // call my_module's func 
    my_module::my_pub_func();
}

