//! A short sentence explaining what my_module is.
//!

/// Describe a public function here. For this example, the func is just to print debug and log message.
///
/// Provide a usage example if necessary
/// ```rust
/// #
///  use example_doc::my_module;
///  my_module::my_pub_func();
/// #
///
pub fn my_pub_func() {
    //short inline comment. - not shown on rust-doc page.
    debug!("inside my_module::my_pub_func(): debug message.");    
    /*
        multi-line comments
        such as private dev note, not shown on rust-doc page:
    */
    info!("inside my_module::my_pub_func(): log message");
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
