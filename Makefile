# cargo new example_doc lib
help:
	grep "^#: " ./Makefile | awk '{ print "   make " $$2 } '

#unset RUST_LOG=trace
export RUST_LOG=trace

#: build
build:
	cargo build

#: clean
clean:
	cargo clean
	rm -rf ./target
	rm -rf doc
	rm -f Cargo.lock

#rustdoc:
#	rustdoc src/lib.rs  crate-name doc_a_lib -o <path>/docs/target/doc -L dependency=<path>/docs/target/debug/deps
#: doc
doc:
	cargo doc
#: doc-open
doc-open:
	 cargo doc open

#: dev-run
dev-run:
	./target/debug/module_doc

#: dev-server
dev-server:
	rm -rf ./server.log 
	./target/debug/my_server > ./server.log 2>&1  &
#: test-server
test-server:	
	curl -X GET http://localhost:8000/help
	curl -X GET http://localhost:8000/plus/2/5
	curl -X GET http://localhost:8000/2/times/5

#: install
install:
	cargo install path .

#: run-server
run-server:
	export RUST_LOG=trace
	rm -rf ./server.log 
	./target/release/my_server  > ./server.log 2>&1  &

#: tests	
tests:
	cargo test
#	cargo test test special_test

.PHONY: help

