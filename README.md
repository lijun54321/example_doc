# example_doc

### General 
- A bare-bone project to demonstrate how to document a rust library and its components including public module and public functions.
- Shelibaron's code documentation requirements is consistent with industry standard (like crate.io). The best practice  include:
	1. Document every library (example in ./src/lib.rs);
	2. Document every module  (example in ./src/my_module.rs); 
	3. Ducument every public functions ( example in ./src/lib.rs, ./src/my_module.rs)

### Requirements
- rustc 1.54.0+ 
- packages (refer to ./Cargo.toml)

### Build, Package, Quick Tests and Deployment
	NOTE: 	If make utility is unavailable, please use original shell commands used in ./Makefile.

- Clone the project:
  	```
	git clone git@gitlab.com:lijun54321/example_doc.git
	```
- Create/update document:
	```
	cd $projectDir;
	make doc       	
	make doc-open  	
	    go to your browser to view the local document, i.e, file:///C:/r/projects/example_doc/target/doc/example_doc/index.html
	```

- Quick Run the example_doc:
	```
	cd $projectDir;
	make build   
	make dev-run

- Start the simple web server and quick tests:
	```	
	cd $projectDir;
	For dev/test etc. purpose:
		make build   
		make dev-server

	For install and run locally: 
		make install
		make run-server		
	
	Quick test the server using curl:
		make tests
		tail -f ./server.log 
	```
